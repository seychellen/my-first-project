import {ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import { HeaderComponent } from './header.component';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HeaderComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('test KopfKnopf', fakeAsync(() => {
    spyOn(component, 'onKopfKnopf');
    component.showKopfKnopf = true;
    fixture.detectChanges(); // <- das hier ist wichtig, dass er die Zeile drüber verarbeitet
    let button = fixture.debugElement.nativeElement.querySelector('.kopf-knopf');
    button.click();
    tick(); // simulates the passage of time until all pending asynchronous activities finish
    expect(component.onKopfKnopf).toHaveBeenCalled();
  }));

})
