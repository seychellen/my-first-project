import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'my-first-project';
  klicktext = 'Klick mich';
  href="http://joerg-vollmer.de";

  sayHello() {
    alert("Hallo Peter");
  }
}
